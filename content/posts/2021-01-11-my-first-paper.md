---
title: My first paper is out!
author: Simone
date: 2021-01-11
---

After many months of work, I am finally happy to announce that my first paper _Green algae scatter off sharp viscosity gradients_ has now been published in __Nature Scientific Reports__.

In the paper, we investigate how the green alga _Chlamydomonas reinhardtii_ behaves when swimming in fluids of increasing viscosity, as well as what happens to it when it tries to cross from one region to another, more viscous, region. We also investigate where algae concentrate when they find themselves in neighbouring regions of different viscosity. Interestingly, we find that the algae concentrate in regions of low viscosity due to a scattering effect that occurs at the interface between the two regions which causes them to quickly re-orient and swim away from it.

If you want to read more about it, you can find the paper [here](https://www.nature.com/articles/s41598-020-79887-7).

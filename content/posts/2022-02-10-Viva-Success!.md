---
title: Viva Success!
author: Simone
date: 2022-02-10
comments: true
---
Happy to announce I successfully defended my PhD thesis yesterday, passing my viva with minor corrections!

Thanks to my examiners, [Dr. Petr Denissenko](https://warwick.ac.uk/fac/sci/eng/people/petr_denissenko/) and [Dr. Mitya Pushkin](https://www.york.ac.uk/maths/staff/dmitri-pushkin/) , for a very interesting discussion and the feedback on my thesis.
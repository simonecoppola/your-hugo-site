---
title: "Second Paper Accepted!"
author: Simone
date: 2021-06-24
---

I am happy to announce that the second paper from my PhD has been accepted for publication!

More info on this project to come when the paper is published.. watch this space!

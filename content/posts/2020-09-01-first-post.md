---
title: The website is live!
author: Simone
date: 2020-09-30
comments: true
---
Welcome to my research website! As mentioned extensively in other pages, this website has been created with the purpose to show my current research, so feel free to go have a look around!

The website is still a work in progress, but hopefully in the future I will be adding more information as well as more updates about my research.

Thanks again for visiting and I hope you enjoy your stay here!

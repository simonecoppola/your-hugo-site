---
author: Simone
title: "Thesis Submitted!"
date: 2021-12-04
---
After a long journey lasted 3 and a half years, with many ups and downs – I’m happy to say I have finally submitted my PhD Thesis!

Each day of the PhD came with its unique challenges, but I am happy I decided to take this path at the end of my BSc in 2018. Through those challenges I was able to develop a lot of skills and meet many people who I am now grateful to be able to call my friends.

Although each PhD comes with its unique challenges on both a professional and personal level, I think some advice applies to anyone about to start a PhD. Or at the very least, I thought it might be a nice idea to write down here some stuff I wish I figured out earlier on during the past 3.5 years:

1. Writing is just as important as being skilled in programming, data analysis or lab techniques. Practice the skill as much as you can and do yourself a favour, pick a copy of The Scientist’s Guide to Writing by Stephen B. Heard as soon as you can.
2. Related to the previous advice, it’s never too early to start writing a paper or a thesis. Even jotting down a draft of the Methodology or Theory/Introductory section for a given project can go a long way in saving you time further down the line. Even if you end up scratching that all together, it’s still excellent practice.
3. Take advantage of having more experienced people than you in the lab/office. Keep your ears open. Listen and learn everyday. Worst case scenario you learn something cool, even if you don’t end up using it down the line.
4. Don’t be afraid to take on new challenges. Whether it’s arranging a talk by an external speaker, giving a presentation about your work or getting involved with some new project. Those experiences can be greatly beneficial…
5. …but don’t take it too far! You don’t have unlimited energy resources, and it’s important not to spread yourself too thin. It might take some trial and error and first, but you’ll get there.
6. Similarly, take care of yourself! Try to eat good (“real”) food and not just easy options (i.e. snacks and fast/frozen food). Food is fuel for your body and mind. Don’t use bad fuel. Write, make music, play guitar, run, go out with friends.. whatever helps you be happy!
7. You got this. Some days will be pretty rough, and you’ll feel like giving up. But then better days where things finally come together will come! Be kind to yourself and remember you didn’t get here by accident!

So here it is, things I wish I knew before starting the PhD! Hopefully someone will find them useful, I know I would have!

And now, off to get some rest before I start the next stage of my (academic) life!

-----

If you want to read more about the work I carried out as a PhD student, you can find the papers I published [on my Google Scholar profile](https://scholar.google.com/citations?user=0Qgdc78AAAAJ&hl=en).

---
title: Publications
comments: false
---

for an up-to-date record of my publications check my [Google Scholar profile](https://scholar.google.com/citations?user=0Qgdc78AAAAJ&hl=en)

---




- Coppola, S. and Kantsler, V. Curved ratchets improve bacteria rectification in microfluidic devices _Phys. Rev. E_ __104__, 014602 (2021). https://doi.org/10.1103/PhysRevE.104.014602
- Coppola, S. and Kantsler, V. Green algae scatter off sharp viscosity gradients. _Sci Rep_ __11__, 399 (2021). https://doi.org/10.1038/s41598-020-79887-7

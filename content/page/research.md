---
title:
comments: false
---

The world surrounding us is rich of swimming biological microorganisms. From the algae in the sea to the bacteria in the ponds and our body, going through the spermatozoa which allow fertilisation and life, microswimmers play a key role in many aspects of our life.

![Gif123](img/examplesperm.gif)

A marvel of biological evolution for microorganisms has been the development of microbial motility. In fact, the majority of microorganisms share the ability of being able to swim thanks to whip-like appendages known as flagella (or cilia).

Similarly to all other living organisms, microbes have developed ways to sense whether an environment is favourable for them and respond accordingly. For example, bacteria can sense a nutrient gradient if they are in one, and change their motion to swim towards a food source or away from any toxic environment. The same principle is believed to be one of the main guidance mechanisms which lead the sperm to the egg and allow fertilisation, both in mammalians and not.

Chemical gradients are not the only changes in the environment to which microorganisms can respond. It has been shown that algae can respond to illumination gradients (phototaxis), and both sperm and bacteria can respond to flow gradients (rheotaxis). Similarly, it is possible to observe responses from topographical gradients (topotaxis) and temperature gradients (thermotaxis).

My research focusses on understanding how microorganisms respond to these different types of gradients, and how we can develop microfluidics devices to take advantage of this responses for biomedical and bioengineering purposes.

**Welcome to my page!**

Please check the about and research pages for an overview of my work.

I also keep a blog on this page where you can find more updated information on my recent research as well my interests, with some (hopefully) useful posts about image processing, microfluidics and bacterial motility!